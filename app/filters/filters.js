﻿angular.module('eBookFilters', [])
    .filter('availability', function () {
        return function (isBorrowed) {
            return isBorrowed ? 'unavailable' : 'available';
        };
    })
    .filter('availableText', function () {
        return function (isBorrowed) {
            return isBorrowed ? 'Utlånt' : 'Ledig';
        };
    })
.filter('availableButtonText', function () {
    return function (isBorrowed) {
        return isBorrowed ? 'UTILGJENGELIG' : 'LÅN';
    };
});
