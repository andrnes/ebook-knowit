﻿var app = angular.module("eBookApp", ["ngRoute", "eBookControllers", "eBookFilters", "firebase"]);

app.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/home', {
            templateUrl: 'partials/home.html',
            controller: 'HomeCtrl'
        }).
        when('/books', {
            templateUrl: 'partials/books.html',
            controller: 'BookListCtrl'
        }).
        when('/books/:bookId', {
            templateUrl: 'partials/book-detail.html',
            controller: 'BookDetailCtrl'
        }).
        when('/search', {
            templateUrl: 'partials/search.html',
            controller: 'SearchCtrl'
        }).
        when('/register', {
            templateUrl: 'partials/register.html',
            controller: 'RegisterBookCtrl'
        }).
        otherwise({
            redirectTo: '/home'
        });
  }]);

