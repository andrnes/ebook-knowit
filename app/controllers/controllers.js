﻿var eBookControllers = angular.module('eBookControllers', []);

eBookControllers.controller('BookListCtrl', ['$scope', 'booksService',
  function ($scope, booksService) {
      $scope.books = booksService.getBooks();
  }]);

eBookControllers.controller('BookDetailCtrl', ['$scope', '$routeParams', '$location', 'booksService', 'userService',
  function ($scope, $routeParams,  $location, booksService, userService) {
      var bookId = $routeParams.bookId;

      $scope.book = booksService.getBook(bookId);

      $scope.rentBook = function (bookId, borrower) {
          booksService.rentBook(bookId, borrower);
          $location.path('#/books');
          toastr.success('Boklånet ditt har blitt registrert!');
      }

      $scope.borrower = userService.getDisplayName();
  }]);

eBookControllers.controller('SearchCtrl', ['$scope', 'booksService', 'searchService',
  function ($scope,  booksService, searchService) {
      var queryString = searchService.getQuery();
      if (queryString != "")
          $scope.query = queryString;

      $scope.books = booksService.getBooks();

      $scope.search = function (book) {
          if (!isNaN($scope.query) && $scope.query.length === 13)
              return $scope.query == book.isbn;
          var re = new RegExp($scope.query, "i");
          return !$scope.query || re.test(book.title) || re.test(book.author) || re.test(book.genre);
      };

      $scope.searchLength = function (book) {
          if (!$scope.query)
              return false;
          return $scope.query.length > 2;
      }

  }]);

eBookControllers.controller('NavbarSearchCtrl', ['$scope', '$location', 'booksService', 'searchService',
  function ($scope, $location, booksService, searchService) {

      $scope.books = booksService.getBooks();

      $scope.searchNavbar = function (query) {
          searchService.saveQuery(query);
          $scope.queryNavbar = "";
          $location.path("/search");
      }

  }]);

eBookControllers.controller('RegisterBookCtrl', ['$scope', '$location', 'booksService',
  function ($scope, $location, booksService) {

      $scope.update = function (book) {
          booksService.insertBook(book);
          $location.path('#/books');
          toastr.success('Bok registrert!');
      };
  }]);

eBookControllers.controller('LoginCtrl', ['$scope', '$firebase', '$firebaseSimpleLogin', 'userService',
  function ($scope, $firebase, $firebaseSimpleLogin, userService) {

      var usersRef = new Firebase('https://flickering-inferno-5445.firebaseio.com/users');
      var authClient = new FirebaseSimpleLogin(usersRef, function (error, user) {
          if (error) {
              console.log(error);
          } else if (user) {
              // User has logged in.
              new Firebase('https://flickering-inferno-5445.firebaseio.com/users/' + user.uid).once('value', function (snapshot) {
                  // Search for user information in Firebase.
                  // If search yields no results, store information in Firebase.
                  if (snapshot.val() == null) {
                      usersRef.child(user.uid).set({
                          displayName: user.displayName,
                          provider: user.provider,
                          provider_id: user.id
                      });

                      $scope.displayName = user.displayName;
                      userService.setDisplayName(user.displayName);
                  }
                  else {
                      // User exists in Firebase. Retrieve and use.
                      $scope.displayName = snapshot.val().displayName;
                      userService.setDisplayName(snapshot.val().displayName);
                  }
              });

              $scope.isLoggedIn = true;
          } else {
              // User is logged out
              $scope.displayName = '';
              $scope.isLoggedIn = false;
              userService.setDisplayName('');
          }
      });

      $scope.login = function (provider) {
          authClient.login(provider);
      };

      $scope.logout = function () {
          authClient.logout();
      }

  }]);

eBookControllers.controller('HomeCtrl', ['$scope', '$location',
  function ($scope, $location) {

      $scope.navigateTo = function (url) {
          $location.path(url);
      };

  }]);