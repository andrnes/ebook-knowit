﻿app.service('booksService', ['$firebase', function ($firebase) {

    var booksRef = new Firebase('https://flickering-inferno-5445.firebaseio.com/ebook/books');
    var sync = $firebase(booksRef);

    this.insertBook = function (book) {
        booksRef.child(book.isbn).set({
            title: book.title,
            author: book.author,
            price: book.price,
            genre: book.genre,
            ranking: book.ranking,
            isbn: book.isbn,
            borrower: "",
            isBorrowed: false
        });
    };

    this.getBook = function (isbn) {
        var ref = booksRef.child(isbn);
        var sync = $firebase(ref);

        return sync.$asObject();
    };

    this.rentBook = function (isbn, borrower) {
        booksRef.child(isbn).update({
            borrower: borrower,
            isBorrowed: true
        });
    }

    this.returnBook = function (isbn) {
        booksRef.child(isbn).update({
            borrower: "",
            isBorrowed: false
        });
    }

    this.getBooks = function () {
        // Added a more visual indication that an object has been changed, e.g. a book has been returned or borrowed.
        // The field for the given book's status in the table will flash to indicate change.
        booksRef.on('child_changed', function (snapshot) {
            $('.book-' + snapshot.val().isbn + ' td:nth-child(5)').fadeOut('slow', function () {
                $(".book-" + snapshot.val().isbn + ' td:nth-child(5)').fadeIn('slow');
            });
        });

        return sync.$asArray();
    };
}]);